#!/bin/sh

check_installed() {
  for prog in $@; do
    command -v "$prog" > /dev/null 2>&1 || { echo "Please install $prog"; exit 1; }
  done
}
