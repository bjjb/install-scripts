name=chruby
version=0.3.9
author=postmodern
url=https://github.com/$author/$name
pkg=$name-$version

check_installed curl tar make
curl -L $url/archive/v$version.tar.gz > $pkg.tar.gz
tar xfz $pkg.tar.gz
make -C $pkg install || exit 1

echo '
To enable chruby for all Bash/ZSH users, add the following to /etc/profile.d:

if [ -n "$BASH_VERSION" ] || [ -n "$ZSH_VERSION" ]; then
  source /usr/local/share/chruby/chruby.sh
  source /usr/local/share/chruby/auto.sh
fi
'
