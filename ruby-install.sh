name=ruby-install
version=0.6.1
author=postmodern
url=https://github.com/$author/$name
pkg=$name-$version

check_installed curl tar make
curl -L $url/archive/v$version.tar.gz > $pkg.tar.gz
tar xfz $pkg.tar.gz
make -C $pkg install || exit 1

ruby-install -L
