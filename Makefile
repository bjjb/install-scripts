all: ruby-install chruby

% : %.sh preamble.sh
	cat preamble.sh $< > $@

changelog:
	git log > CHANGELOG

test:
	./ruby-install
	./chruby

.PHONY: all test changelog
